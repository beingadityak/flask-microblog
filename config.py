import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    # Flask-WTF CSRF Secret
    SECRET_KEY = os.environ.get('SECRET_KEY') or 't0p-s3cr3t!'
    # Flask-SQLAlchemy DB Config
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    POSTS_PER_PAGE = 25